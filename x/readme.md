# CS 349 X Windows and XLib Example Code


To make ("compile and link") an example, use the included makefile with 
the name of cpp file passed as a variable. For example, to make hello.cpp:

```bash
	make NAME="hello"
```

Then, to run:

```bash
    ./hello 
```

Or you can even compile, link, and run in one step:

```bash
    make run NAME="hello"
```


## Examples


### Windowing Systems

    hello.cpp           Draws text in window, demos larger font choice.

    null.min.cpp        Creates and destroys a display (a good first test to see
                		if X Windows is working).

    openwindow.min.cpp  Opens a single blank window (minimal version)

	openwindow.cpp     	Opens a single blank window

### Drawing

	drawing.min.cpp	    Demos drawing and graphics context (minimal version)
       
    drawing.cpp     	Demos drawing functions and graphics contexts  
    
    displaylist         Demos Displayable class and display list technique
    
    clipping.cpp        Demos clipping using a mouse controlled clipping
                        rectangle to reveal a pattern of random lines.

### Events

    eventloop.min.cpp   Demos events by displaying mouse motion events 
    					to console (minimal version)

    eventloop.cpp     	Demos events by drawing text at cursor location

    animation.cpp   	Demos animation with using non-blocking
    					event loop to bounce ball around.

    doublebuffer1.cpp   Demos double buffering (using same 
    				    demo as animation.cpp)

    doublebuffer2.cpp   Demos double buffering (using expanded version of 
                        animation.cpp to amplify effect of doublebuffering)
                        
### Widgets

    button.cpp          Implement a ToggleButton UI Widget
    				    
### Extras (not covered in lecture)

    multiwindow         Opens and draws into two windows 
                        (see command line usage when running)

    xeyesball           More complicated animation, event handling, and 
                        drawing.

    xeyesballdb         xeyesball with double buffering



               

