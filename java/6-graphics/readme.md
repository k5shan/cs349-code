## Java graphics demos

> NOTE: These demos require vecmath.jar to be compiled with demo. See the included Makefile. 

* `SimpleDraw.java` Responds to mouse events and draws primitives in 2D canvas.

* `ClosestPoint.java` Computes distance from mouse to closest point on line using algorithm discussed in class. Also uses built-in method to accomplish same thing.

* `PolygonHittest.java` Uses built-in method to hit-test closed polygon.

* `Transform1.java` Shows how to "manually" transform a shape model. NOTE: in practice, you don't want do this. Use Graphics2D matrix tranformations instead.

* `Transform2.java`  Shows how to use Graphics2D matrix tranformations to transform a shape model. 

* `CompositionOrder` Demo of different concatenation orders of matrix transforms. Click the window to change the order.

* `BarExercise` Demo of multiple transformation exercise.

* `/shapedemo` Example shape model (`ShapeDemo.java` and `Shape.java`).

* `SceneGraph.java` Demo of using transformations as a simple scene graph.
				  